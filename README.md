# switch vs check

```bash
git rebase -i HEAD~2

# ---
pick 96ae95f Add b # change pick to edit then :wq
pick b60b852 Add c
# ---

# you would got error if trying to move branch
git switch main

# fatal: cannot switch branch while rebasing
# Consider "git rebase --quit" or "git worktree add".

# success. You must not use this command while git rebasing
git checkout main

# Progress or abort rebase instead of checkout.
git rebase --continue
# or
git rebase --abort
```
